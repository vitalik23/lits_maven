package api;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class TestUkrNet {

    @Test
    public void testUkrNet() {
        given().when().get("http://www.ukr.net").then().statusCode(200);
    }
}
