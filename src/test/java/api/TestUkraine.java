package api;

import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

public class TestUkraine extends BaseTest {

    @Test
    public void getRequestFindCapital() throws JSONException {

        Response response = get("name/ukraine");

        JSONArray jsonResponseAsArray = new JSONArray(response.asString());
        JSONObject jsonResponseAsJson = jsonResponseAsArray.getJSONObject(0);

        System.out.println(jsonResponseAsJson);

        System.out.println(response.asString());

        response.then().body("name", equalTo("Ukraine"));
//        response.then().body("capital", equalTo("Kiev"));
    }
}
//https://semaphoreci.com/community/tutorials/testing-rest-endpoints-using-rest-assured

//https://stackoverflow.com/questions/16518252/different-json-array-response?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa