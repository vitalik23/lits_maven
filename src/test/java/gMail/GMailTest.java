package gMail;

import demoqa.BaseTest;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import selenium_1.domain.User;
import selenium_1.pages.LoginPage;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;

public class GMailTest extends BaseTest {

    @Test
    public void testCanLogin() {

        List<WebElement> emailsSubjects = new LoginPage()
                .open()
                .loginAs(new User("testerrp111", "ntcnthhg11"))
                .getEmailsSubjects();

        if (emailsSubjects != null) {
            List<String> subjects = new ArrayList<>();

            for (WebElement email : emailsSubjects) {
                subjects.add(email.getText());
                System.out.println(email.getText());
            }
            assertThat(subjects, hasItem("Hello User"));
        }
    }
}