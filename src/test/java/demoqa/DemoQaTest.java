package demoqa;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import selenium_3.domain.User;
import selenium_3.pages.*;
import utils.DriverUtil;
import utils.JsonUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static utils.ScreenshotUtil.takeScreenshot;

public class DemoQaTest extends BaseTest {

    private HomePage homePage;
    private FramesAndWindowsPage framesAndWindowsPage;

    //    TODO Fix error: java.lang.ClassCastException: selenium_3.domain.User cannot be cast to [Ljava.lang.Object;
    @DataProvider(name = "usersFromJson")
    public static Iterator<Object[]> usersFromJson() {
        return JsonUtil.passeJSON().iterator();
    }

    //    Data provider as list iterator
    @DataProvider(name = "usersIterator")
    public static Iterator<Object[]> usersIterator() {
        List<Object[]> users = new ArrayList<>();
        users.add(new User[]{
                new User(
                        "Rico",
                        "Parisian",
                        new String[]{"Reading", "Cricket", "Dance"},
                        "600021438331",
                        "karine82",
                        "Landen99@mail.com",
                        "?rmn3=d7iJunQ,!Q%*")
        });
        users.add(new User[]{
                new User(
                        "Selena",
                        "Leuschke",
                        new String[]{"Cricket", "Dance"},
                        "04975936262",
                        "hkoss",
                        "cronin.barton@mail.com",
                        "V7{70pq'_4n|u])8")
        });
        users.add(new User[]{
                new User(
                        "Ms. Marisa",
                        "Bernhard V",
                        new String[]{"Reading", "Dance"},
                        "08760148739",
                        "jconsidine",
                        "aucek@mail.com",
                        "(H6)bxn]RQCi4LAj}LxA")
        });
        users.add(new User[]{
                new User(
                        "Elna",
                        "Schmeler",
                        new String[]{"Reading", "Cricket"},
                        "391521196945796",
                        "howellalaina",
                        "titis007@igg.biz",
                        "mY$Kc%K$ufhsPa0PUmk")
        });
        users.add(new User[]{new User(
                "Tressa",
                "Olson",
                new String[]{"Dance", "Reading"},
                "399395536477200",
                "vwuckert",
                "tiara67@wunsch.com",
                "t&8lP(RN[lXZJQ")});
        return users.iterator();
    }

    //    Data provider as array of objects
    @DataProvider(name = "users")
    public static Object[][] users() {
        return new Object[][]{{
                new User(
                        "Rico",
                        "Parisian",
                        new String[]{"Reading", "Cricket", "Dance"},
                        "0021438331",
                        "karine82",
                        "Landen99@mail.com",
                        "?rmn3=d7iJunQ,!Q%*")
        }, {
                new User(
                        "Selena",
                        "Leuschke",
                        new String[]{"Cricket", "Dance"},
                        "04975936262",
                        "hkoss",
                        "cronin.barton@mail.com",
                        "V7{70pq'_4n|u])8")
        }, {
                new User(
                        "Ms. Marisa",
                        "Bernhard V",
                        new String[]{"Reading", "Dance"},
                        "08760148739",
                        "jconsidine",
                        "aucek@mail.com",
                        "(H6)bxn]RQCi4LAj}LxA")
        }, {
                new User(
                        "Elna",
                        "Schmeler",
                        new String[]{"Reading", "Cricket"},
                        "391521196945796",
                        "howellalaina",
                        "titis007@igg.biz",
                        "mY$Kc%K$ufhsPa0PUmk")
        }, {
                new User(
                        "Tressa",
                        "Olson",
                        new String[]{"Dance", "Reading"},
                        "39939536477200",
                        "vwuckert",
                        "tiara67@wunsch.com",
                        "t&8lP(RN[lXZJQ")
        }
        };
    }

    @BeforeClass
    public void setUp() {
        homePage = new HomePage().open();
    }

    @Test(dataProvider = "usersIterator")
//    @Test(dataProvider = "users")
//    @Test(dataProvider = "usersFromJson")
    public void testCanRegisterNewUser(User user) {
//        Registration process (positive scenario). Use data provider for user data.
        WebElement successMessage =
                homePage
                        .openRegistrationPage()
                        .registerNewUser(user)
                        .getSucsessMessage();

        assertThat(
                successMessage.getText(),
                equalTo("Thank you for your registration"));
    }

    @Test
    public void testCanBoxDragging() {
        int x = 200;
        int y = 200;
//        int distance = (int) Math.sqrt(x*x + y*y);
        DraggablePage page = homePage
                .openDraggablePage();
        Point beforeBoxLocation =
                page.getDraggableBoxLocation();
        takeScreenshot();
        page.dragBoxTo(x, y);
        Point afterBoxLocation =
                page.getDraggableBoxLocation();
        takeScreenshot();

        assertThat(
                afterBoxLocation.getX(),
                equalTo(beforeBoxLocation.getX() + x));
        assertThat(
                afterBoxLocation.getY(),
                equalTo(beforeBoxLocation.getY() + y));
    }

    @Test
    public void testCanSelectGroupOfElements() {
        SelectablePage page = homePage
                .openSelectablePage();
        page.selectElementsInRange(3, 7);
        takeScreenshot();
        assertThat(page.getCountOfSelectedItems(), equalTo(5));

        page.selectElementsFromFirstTo(7);
        takeScreenshot();
        assertThat(page.getCountOfSelectedItems(), equalTo(7));
    }

    @BeforeGroups(groups = "framesAndWindows")
    public void beforeGroup() {
        framesAndWindowsPage = homePage.openFramesAndWindowsPage();
    }

    @Test(groups = "framesAndWindows")
    public void testCanOpenNewTab() {
        framesAndWindowsPage
                .openNewTab()
                .switchToTheNewTab();

        FramesAndWindowsPage newTab =
                PageFactory.initElements(DriverUtil.getDriver(), FramesAndWindowsPage.class);
        String newTabUrl =
                newTab.getCurrentUrl();
        newTab
                .closeCurrentTab();
        framesAndWindowsPage
                .switchToTheOldTab();

        assertThat(
                newTabUrl, equalTo("http://demoqa.com/frames-and-windows/#"));
        assertThat(
                framesAndWindowsPage.getCurrentUrl(), equalTo("http://demoqa.com/frames-and-windows/"));
    }

    @Test(groups = "framesAndWindows")
    public void testCanOpenNewWindow() {
        framesAndWindowsPage
                .openNewWindow()
                .switchToTheNewTab();
        BasePage newWindow = new BasePage();
        String newWindowUrl =
                newWindow.getCurrentUrl();
        String newWindowTitle = newWindow.getPageTitle();
        newWindow
                .closeCurrentTab();
        framesAndWindowsPage
                .switchToTheOldTab();

        assertThat(
                newWindowUrl, equalTo("http://toolsqa.com/registration"));
        assertThat(
                newWindowTitle, equalTo("Page Not Found | TOOLSQA"));
        assertThat(
                framesAndWindowsPage.getCurrentUrl(), equalTo("http://demoqa.com/frames-and-windows/"));
    }


    @Test(groups = "framesAndWindows")
    public void testCanSwitchBetweenFrames() {
        framesAndWindowsPage
                .openNewTabWithFrames()
                .switchToTheNewTab();
        FramesPage framesPage =
                PageFactory.initElements(DriverUtil.getDriver(), FramesPage.class);

        assertThat(
                framesPage.getFramesCount(), equalTo(3));
        assertThat(
                framesPage.getFirstFrameBgColor(), equalTo("rgba(151, 163, 193, 1)"));
        assertThat(
                framesPage.getSecondFrameBgColor(), equalTo("rgba(102, 153, 102, 1)"));
        assertThat(
                framesPage.getThirdFrameBodyClass(), equalTo("error404"));

        framesPage
                .closeCurrentTab();
        framesAndWindowsPage
                .switchToTheOldTab();
    }



}
