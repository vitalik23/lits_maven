package selenium_3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.DriverUtil;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.numberOfElementsToBeMoreThan;

public class SelectablePage extends BasePage {
    Actions builder = new Actions(DriverUtil.getDriver());

    @FindBy(css = "#selectable li")
    List<WebElement> elements;

    @FindBy(css = "#selectable li.ui-selected")
    List<WebElement> selectedList;

    public void selectElementsFromFirstTo(int count) {
        selectElementsInRange(1, count);
    }

    public void selectElementsInRange(int first, int last) {
        if ((first > 0) & (first < last)) {
            if ((last > first) & (last <= elements.size())) {
                builder
                        .clickAndHold(elements.get(first - 1))
                        .moveToElement(elements.get(last - 1))
                        .release()
                        .build().perform();
            }
        } else {
            System.err.println(String.format("Out of elements range. List size is %d", elements.size()));
        }

    }

    public int getCountOfSelectedItems() {
        new WebDriverWait(DriverUtil.getDriver(), 5)
                .until(numberOfElementsToBeMoreThan(By.cssSelector("li.ui-selected"), 0));
        return selectedList.size();
    }
}
