package selenium_3.pages;

/* Created by vitaliismokov in 03.06.18
https://github.com/vitalik23 */


import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class DraggablePage extends BasePage {

    @FindBy(id = "draggable")
    WebElement draggableBox;


    public void dragBoxTo(int x, int y) {
        new Actions(driver).dragAndDropBy(draggableBox, x, y)
                .build().perform();
    }

    public Point getDraggableBoxLocation() {
        return draggableBox.getLocation();
    }
}
