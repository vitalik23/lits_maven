package selenium_3.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.DriverUtil;
import utils.WaitUtil;

import java.util.List;

public class BasePage {

    protected WebDriver driver = DriverUtil.getDriver();

    protected void typeInto(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);
    }

    protected WebElement element(String locator) {
        return WaitUtil.waitForVisibleElement(locator);
    }

    protected List<WebElement> elements(String locator) {
        return WaitUtil.waitForVisibleElements(locator);
    }

    public String getCurrentUrl(){
        return driver.getCurrentUrl();
    }

    public String getPageTitle(){
        return driver.getTitle();
    }

    public void closeCurrentTab() {
        driver.close();
    }


    //    public WebElement getElement(String selector){
//        return new FluentWait<>(driver)
//                                .pollingEvery(100, TimeUnit.MILLISECONDS)
//                                .ignoring(NoSuchElementException.class)
//                                .withTimeout(20, TimeUnit.SECONDS)
//                                .withMessage("Couldn't find element even after 20 seconds")
//                                .until(webElement -> driver.findElement(By.cssSelector(selector)));
//    }

}
