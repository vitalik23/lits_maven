package selenium_3.pages;

/* 
Created by vitaliismokov in 04.06.18
https://github.com/vitalik23
*/

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;

public class FramesAndWindowsPage extends BasePage {

    private String currentTabHandle;
    private ArrayList<String> tabs2;

    @FindBy(id = "ui-id-1")
    private WebElement openNewWindowTab;

    @FindBy(css = "#tabs-1 a")
    private WebElement openNewWindowLink;

    @FindBy(id = "ui-id-2")
    private WebElement openSeparateNewWindowTab;

    @FindBy(css = "#tabs-2 a")
    private WebElement openSeparateNewWindowLink;

    @FindBy(id = "ui-id-3")
    private WebElement frameSetTab;

    @FindBy(css = "#tabs-3 a")
    private WebElement openFrameSetWindowLink;


    public FramesAndWindowsPage openNewTab() {
        if (!openNewWindowTab.getAttribute("class").equals("ui-tabs-active")) {
            openNewWindowTab.click();
        }
        openNewWindowLink.click();
        return this;
    }

    public FramesAndWindowsPage openNewWindow() {
        if (!openSeparateNewWindowTab.getAttribute("class").equals("ui-tabs-active")) {
            openSeparateNewWindowTab.click();
        }
        openSeparateNewWindowLink.click();
        return this;
    }

    public FramesAndWindowsPage openNewTabWithFrames() {
        if (!frameSetTab.getAttribute("class").equals("ui-tabs-active")) {
            frameSetTab.click();
        }
        openFrameSetWindowLink.click();
        return this;
    }

    public void switchToTheNewTab() {
        currentTabHandle = driver.getWindowHandle();
        tabs2 = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
    }

    public void switchToTheOldTab() {
        driver.switchTo().window(currentTabHandle);
    }
}
