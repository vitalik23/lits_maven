package selenium_3.pages;

import org.openqa.selenium.WebElement;
import selenium_3.domain.User;

public class RegistrationPage extends BasePage {

    public RegistrationPage registerNewUser(User user) {

        typeInto(element("#name_3_firstname"), user.getFirstName());
        typeInto(element("#name_3_lastname"), user.getLastName());
        selectHobby(user.getHobby());
        typeInto(element("#phone_9"), user.getPhoneNumber());
        typeInto(element("#username"), user.getUsername() + System.currentTimeMillis());
        typeInto(element("#email_1"), System.currentTimeMillis() + user.geteMail());
        typeInto(element("#password_2"), user.getPassword());
        typeInto(element("#confirm_password_password_2"), user.getPassword());
        element("input[name='pie_submit']").click();
        return this;
    }

    public WebElement getSucsessMessage() {
        return element("p.piereg_message");
    }

    private void selectHobby(String[] hobbies) {
        for (String hobby : hobbies) {
            if (hobby != null) {
                switch (hobby.toLowerCase()) {
                    case "dance":
                        element("input[value = 'dance']").click();
                        break;

                    case "reading":
                        element("input[value = 'reading']").click();
                        break;

                    case "cricket":
                        element("input[value = 'cricket ']").click();
                        break;

                    default:
                        element("input[value = 'dance']").click();
                        break;
                }
            } else
                System.err.println("Hobby is not provided");

        }
    }
}
