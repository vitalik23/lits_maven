package selenium_3.pages;

import org.openqa.selenium.support.PageFactory;
import utils.DriverUtil;

public class HomePage extends BasePage {


    public HomePage open() {
        DriverUtil.getDriver().navigate().to("http://demoqa.com");
        return this;
    }

    public RegistrationPage openRegistrationPage() {
        element("#menu-item-374").click();
        return new RegistrationPage();
    }

    public DraggablePage openDraggablePage() {
        element("#menu-item-140").click();
        return PageFactory.initElements(driver, DraggablePage.class);
    }

    public SelectablePage openSelectablePage() {
        element("#menu-item-142").click();
        return PageFactory.initElements(driver, SelectablePage.class);
    }

    public FramesAndWindowsPage openFramesAndWindowsPage() {
        element("#menu-item-148").click();
        return PageFactory.initElements(driver, FramesAndWindowsPage.class);
    }

}
