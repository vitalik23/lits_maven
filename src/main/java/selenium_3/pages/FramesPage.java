package selenium_3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/*
Created by vitaliismokov in 06.06.18
https://github.com/vitalik23
*/
public class FramesPage extends BasePage {

    @FindBy(name = "topFrame")
    private WebElement firstFrame;

    @FindBy(name = "contentFrame")
    private WebElement secondFrame;

    @FindBy(css = "frame[src='UntitledFrame-7']")
    private WebElement thirdFrame;

    public int getFramesCount() {
        return driver.findElements(By.tagName("frame")).size();
    }

    public void switchToFrameWithName(String name) {
        driver.switchTo().frame(name);
    }

    public String getFirstFrameBgColor() {
        String color = switchToFrame(firstFrame)
                .findElement(By.tagName("body")).getCssValue("background-color");
        switchBackToThePage();
        return color;
    }

    private void switchBackToThePage() {
        driver.switchTo().defaultContent();
    }

    public String getSecondFrameBgColor() {
        String color = switchToFrame(secondFrame)
                .findElement(By.tagName("body")).getCssValue("background-color");
        switchBackToThePage();
        return color;
    }

    public String getThirdFrameBodyClass() {
        String bodyClass = switchToFrame(thirdFrame)
                .findElement(By.tagName("body")).getAttribute("class");
        switchBackToThePage();
        return bodyClass;
    }

    public WebDriver switchToFrame(WebElement frame) {
        return driver.switchTo().frame(frame);
    }

}
