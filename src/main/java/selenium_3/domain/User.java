package selenium_3.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    private String firstName;
    private String lastName;
    private String[] hobby;
    private String phoneNumber;
    private String username;
    private String eMail;
    private String password;

    @JsonCreator
    public User(
            @JsonProperty("first_name") String firstName,
            @JsonProperty("last_name") String lastName,
            @JsonProperty("hobby") String[] hobby,
            @JsonProperty("phone_number") String phoneNumber,
            @JsonProperty("username") String username,
            @JsonProperty("e-mail") String eMail,
            @JsonProperty("password") String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.hobby = hobby;
        this.phoneNumber = phoneNumber;
        this.username = username;
        this.eMail = eMail;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String[] getHobby() {
        return hobby;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public String geteMail() {
        return eMail;
    }

    public String getPassword() {
        return password;
    }
}
