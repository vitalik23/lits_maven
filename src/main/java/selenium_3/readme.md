Завдання на Selenium #3

# Open http://demoqa.com/. Write automation scripts that verify:

1. ~~Registration process (positive scenario).
Use data provider for user data.~~
2. Mandatory fields and data validation on registration page (negative
scenario). Use json parsing for invalid data.
3. ~~Box dragging on the 'Draggable' page.
Make screenshots before andafter dragging.~~
4. ~~Selecting multiple options on the 'Selectable' page.
Make screenshot of all the options selected.~~
5. Switching between frames and browser tabs on 'Frames and windows' page.

- all the tests should be organised in a single project
- use maven to build your project