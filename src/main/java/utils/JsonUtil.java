package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import selenium_3.domain.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class JsonUtil {

    public static ArrayList<Object[]> passeJSON() {

        FileInputStream file = null;
        try {
            file = new FileInputStream("src/test/resources/testData/users.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ObjectMapper mapper = new ObjectMapper();
        TypeFactory factory = TypeFactory.defaultInstance();

        ArrayList<Object[]> list = null;
        try {
            list = mapper.readValue(
                    file, factory.constructCollectionType(
                            ArrayList.class, User.class));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Object o : list) {
            System.out.println(o);
            System.out.println("*************************");
        }

        return list;
    }
}
