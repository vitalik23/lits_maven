package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomStringGenerator {

    Random random = new Random();

    public List<String> generateRandomStringList(int listLength) {

        final char[] alphabet = {
                'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
                'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
                'z', 'x', 'c', 'v', 'b', 'n', 'm'
        };

        List<String> list = new ArrayList<>();

        for (int i = 0; i < listLength; i++) {

            int stringLength = random.nextInt(10) + 6;
            StringBuilder generatedString = new StringBuilder();

            while (stringLength-- > 0) {
                generatedString.append(alphabet[random.nextInt(alphabet.length)]);
            }

            list.add(generatedString.toString());
        }

        return list;
    }
}
