package tasks6_8.interfaces;

import tasks6_8.classes.phones.Contact;

public interface IPhone {

    void call(Contact contact);

    void sentSMS(Contact contact, String text);

}
