package tasks6_8;

//Create class with 4 (or more) class fields. Generate all getters and setters.
// Write 3 (or more) own class methods with different return types.
// Create two extended classes with the same options as parent class.
//Create the container(Collection) of your class objects and iterate it, calling some overridden method.
//Create a simple interface with 1 (or more) methods and implement it by the child class. Also try to use
// different (same) packages.
//[Optional] Create static method in parent class and try to use it in main method.
// (syntax: ClassName.methodName() )
//[Optional] Try to use final method in parent class and override it in one of the  subclasses.
//[Optional] Make the parent class as abstract and create the object of that class in main method.
//TODO
//[Optional] Try to use different packages and access modifiers.
//
//*Створити проект і в ньому окремий package з окремими класами. Клас в якому буде main метод повинен бути
// в окремому пекеджі. Експортувати проект та заархівувати. Створений архів приатачити до системи moodle.
// У назві файлу вказати своє прізвище та номер завдання.

import tasks6_8.classes.AbstractClass;
import tasks6_8.classes.Car;
import tasks6_8.classes.Human;
import tasks6_8.classes.accessModifiers.AccessModifiersClass;
import tasks6_8.classes.accessModifiers.AccessModifiersClassHelper;
import tasks6_8.classes.phones.CellPhone;
import tasks6_8.classes.phones.Contact;
import tasks6_8.classes.phones.Smartphone;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Human vitalik = new Human("Vitalik", 35, "Naukova 43/16");
        // Write 3 (or more) own class methods with different return types.
        System.out.println(vitalik.introduceMyself());
        printAsterisks(100);
        System.out.println(Arrays.toString(vitalik.countTo(10)));
        printAsterisks(100);
        System.out.println(vitalik.buyNewCar("Mercedes-Benz E-Class E 550", "black"));
        printAsterisks(100);

        //Create the container(Collection) of your class objects and iterate it, calling some overridden method.
        List<Car> collection = Arrays.asList(
                new Car("BMW 7 Series", 2012),
                new Car("Cadillac Escalade", 2009),
                new Car("Dodge Challenger SXT Plus", 2013),
                new Car("GMC Yukon XL SLT 1500", 2014));
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next().toString());
            printAsterisks(100);
        }


        Smartphone smartphone = new Smartphone();
        Contact contact = new Contact("+380639904546", "Vitalii", "Smokov");
        printAsterisks(100);
        smartphone.call(contact);
        smartphone.sentSMS(contact, "Hello my friend");
        smartphone.openWebPage("www.google.com");
        smartphone.sentEmail("vitalik23@ukr.net",
                "Data is transmitted in units of 4 bits (1 nibble) for which the interval between\n" +
                        "two falling edges (single edge) of the modulated signal with a constant amplitude\n" +
                        "voltage is evaluated. A SENT message is 32 bits long (8 nibbles) and consists of\n" +
                        "the following components: 24 bits of signal data (6 nibbles) that represents 2\n" +
                        "measurement channels of 3 nibbles each (such as pressure and temperature), 4 bits\n" +
                        "(1 nibble) for CRC error detection, and 4 bits (1 nibble) of status/communication\n" +
                        "information. Optionally, data can be transferred in 20 bit messages (5 nibbles)\n" +
                        "composed of a single 12 bit (3 nibble) measurement, a 4 bit (1 nibble) CRC error\n" +
                        "checksum, and a 4 bit (1 nibble) status/communication field.");

        printAsterisks(100);

        //[Optional] Create static method in parent class and try to use it in main method.
        System.out.println(String.format("Total contacts is: %d", Contact.howMutchContacts()));
        printAsterisks(100);

        //[Optional] Try to use final method in parent class and override it in one of the  subclasses.
        CellPhone cellPhone = new CellPhone();
        cellPhone.ring();
        cellPhone.ringCell();

        printAsterisks(100);

        //[Optional] Make the parent class as abstract and create the object of that class in main method.
        AbstractClass absClass = new AbstractClass() {
            @Override
            public void abstractClassMethod() {
//                super.abstractClassMethod();
                System.out.println("Method of abstract class");
            }
        };
        absClass.abstractClassMethod();

        //[Optional] Try to use different packages and access modifiers.
        printAsterisks(100);
        AccessModifiersClass accessModifiersClass = new AccessModifiersClass();
        AccessModifiersClassHelper accessModifiersClassHelper = new AccessModifiersClassHelper(accessModifiersClass);

        accessModifiersClass.setPrivateField("Private");
        accessModifiersClassHelper.setPackageField("Package");
        accessModifiersClassHelper.setProtectedField("Protected");
        accessModifiersClass.publicField = "Public";

        System.out.println(String.format("%s class field", accessModifiersClass.getPrivateField()));
        System.out.println(String.format("%s class field", accessModifiersClassHelper.getPackageField()));
        System.out.println(String.format("%s class field", accessModifiersClassHelper.getProtectedField()));
        System.out.println(String.format("%s class field", accessModifiersClass.publicField));
    }

    private static void printAsterisks(int quantity) {
        int i = 1;
        while (i <= quantity) {
            System.out.print("*");
            i++;
        }
        System.out.println();
    }
}
