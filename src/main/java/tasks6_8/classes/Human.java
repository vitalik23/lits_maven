package tasks6_8.classes;

import java.util.Random;

public class Human {

    private int age;
    private String name;
    private String address;
    private Car car;

    public Human() {
        this.age = new Random().nextInt();
        this.name = "John Doe";
        this.address = "Homeless";
    }

    public Human(String name) {
        this.name = name;
        this.age = new Random().nextInt();
        this.address = "Homeless";
    }

    public Human(String name, int age) {
        this(name);
        this.age = age;
        this.address = "Homeless";
    }

    public Human(String name, int age, String address) {
        this(name, age);
        this.address = address;
    }

    public Human(String name, int age, String address, Car car) {
        this(name, age, address);
        this.car = car;
    }


    public String introduceMyself() {
        return String.format("Hello, my name is %s.\nI am %d years old.\nMy address is: %s.",
                this.name, this.age, this.address);
    }

    public Car buyNewCar(String model, String color) {
        return new Car(model, color, this);
    }

    public int[] countTo(int number) {
        int[] array = new int[number];
        for (int i = 0; i < array.length; i++)
            array[i] = i + 1;
        return array;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
