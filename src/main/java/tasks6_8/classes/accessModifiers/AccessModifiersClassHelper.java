package tasks6_8.classes.accessModifiers;

public class AccessModifiersClassHelper {

    AccessModifiersClass accessModifiersClass;

    public AccessModifiersClassHelper(AccessModifiersClass accessModifiersClass) {
        this.accessModifiersClass = accessModifiersClass;
    }

    public String getPackageField() {
        return accessModifiersClass.packageField;
    }

    public void setPackageField(String string) {
        accessModifiersClass.packageField = string;
    }

    public String getProtectedField() {
        return accessModifiersClass.protectedField;
    }

    public void setProtectedField(String string) {
        accessModifiersClass.protectedField = string;
    }
}
