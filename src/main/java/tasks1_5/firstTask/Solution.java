package tasks1_5.firstTask;

import utils.ListUtil;

import java.util.List;

//1. Написати програму для роботи з списком.
// У першій половині списку замінити всі входження деякого елементу Е1 на
//  будь-який інший елемент Е2;

public class Solution {

    public static void main(String[] args) {
        List<Integer> list = new ListUtil().generateRandomIntList(10, 10);
        changeElementInList(list, 5, 0);
    }

    private static void changeElementInList(List<Integer> list, int elementToChange, int newElement) {
        for (int i = 0; i < list.size() / 2; i++) {
            if (list.get(i) == elementToChange) {
                list.remove(i);
                list.add(i, newElement);
            }
        }
    }

}
