package tasks1_5.secondTask;

//2. Задано масив цілих чисел. Вивести масив в оберненому порядку, а потім видалити з нього
// повторні входження кожного елемента.

import utils.ArrayUtil;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class Solution2 {

    public static void main(String[] args) {

        int[] array = new ArrayUtil().generateRandomIntArray(10, 10);

        System.out.println(Arrays.toString(reverseArray(array)));

        removeRecurringElements(reverseArray(array));
    }

    private static int[] reverseArray(int[] array) {

        int[] reversedArray = new int[array.length];

        for (int i = array.length - 1; i >= 0; i--) {
            reversedArray[(array.length - 1) - i] = array[i];
        }

        return reversedArray;
    }

    private static int[] removeRecurringElements(int[] array) {

        Set<Integer> set = new LinkedHashSet<>();

        for (int element : array) {
            set.add(element);
        }

        int[] clearArray = new int[set.size()];
        int i = 0;

        for (int element : set) {
            clearArray[i++] = element;
        }

        return clearArray;
    }
}
