package selenium_1.domain;

public enum Browser {
    CHROME,
    FIREFOX,
    EDGE,
    IE
}
