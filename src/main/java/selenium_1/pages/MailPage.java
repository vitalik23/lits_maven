package selenium_1.pages;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import selenium_3.pages.BasePage;

import java.util.List;

public class MailPage extends BasePage {

    public List<WebElement> getEmailsSubjects() {
        List<WebElement> letters = null;
        try {
            letters = elements("#\\3a 3d");
        } catch (TimeoutException e) {
            System.err.println("Letters not found");
        }
        return letters;
    }
}
