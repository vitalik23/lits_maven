package selenium_1.pages;

import selenium_1.domain.User;
import selenium_3.pages.BasePage;

public class LoginPage extends BasePage {


    public LoginPage open() {
        driver.get("https://www.google.com/gmail/");
        return this;
    }

    public MailPage loginAs(User user) {
        typeInto(element("#identifierId"), user.getEmail());
        element("#identifierNext").click();
        typeInto(element("input[type='password']"), user.getPassword());
        element("#passwordNext").click();
        return new MailPage();
    }

}
