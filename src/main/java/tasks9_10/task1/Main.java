package tasks9_10.task1;

//1) Встановити MySql Server, налаштувати db connection & db schema. Створити таблиці та заповнити їх
// даними. Засобами jdbc підєднатись до створеної таблиці та отримати з неї дані.
//

import java.sql.ResultSet;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {
        DBDriverHelper db = new DBDriverHelper();
        ResultSet resultSet = db.executeQuery(
                "SELECT * FROM sakila.address\n" +
                        "where district like \"California\""
        );
        System.out.println("\nRESULTS :");
        System.out.println("Postal code\t\tPhone \t\t\tAddress");
        try {
            while (resultSet.next()) {
                System.out.println(String.format("%s\t\t\t%s\t%s",
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("address")));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        db.closeAllDBConnections();
    }
}
