package tasks9_10.task4;

//4) Створіть json файл, засобами Java (jackson) розпарсайте його. Використайте DTO data binding
// (mapping). json повинен починатись з "{"


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        FileInputStream file = new FileInputStream("src/main/java/tasks9_10/task4/json_data/users.json");

        ObjectMapper mapper = new ObjectMapper();
        TypeFactory factory = TypeFactory.defaultInstance();

        List<User> userList = mapper.readValue(
                file, factory.constructCollectionType(
                        ArrayList.class, User.class));

        for (User user : userList) {
            System.out.println(user);
            System.out.println("*************************");
        }
    }
}