package tasks9_10.task3;

//3) Напишіть метод, який зчитує дані з файлу у HashMap, ключем якої є номер рядку файлу, а value -
// це текст у рядку файлу. Запишіть у інший файл тільки ті значення HashMap, ключі яких є степенем 2.
// Використайте перехоплення виключення ситуацій. (IOException)

import utils.TextReader;

import java.util.Map;

public class Main {

    public static void main(String[] args) {

        System.out.println("\n3) Read data from file: ");
        Map<Integer, String> mapFromFile = new TextReader().readFileToHashMap("text_data.txt");
        MapUtil mapUtil = new MapUtil();
        mapUtil.printMapValues(mapFromFile);
        Map<Integer, String> filteredMap = mapUtil.filterMapByPowerOfNumberTwo(mapFromFile);
        mapUtil.printMapValues(filteredMap);

    }
}
