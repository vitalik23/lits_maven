package tasks9_10.task3;

import java.util.HashMap;
import java.util.Map;

public class MapUtil {

    public Map filterMapByPowerOfNumberTwo(Map<Integer, String> map) {
        Map<Integer, String> filteredMap = new HashMap();

        for (int index : map.keySet()) {
            if ((index > 0) && ((index & (index - 1)) == 0))
                filteredMap.put(index, map.get(index));
        }
        return filteredMap;
    }

    public void printMapValues(Map<Integer, String> map) {
        for (String s : map.values())
            System.out.println(s);
    }
}
