package tasks9_10.task2;

import utils.RandomStringGenerator;

import java.util.Calendar;
import java.util.Date;

//2) "Використання класів Random, Date/Calendar"
//
//У циклі та з колекціями (згенерувати випадкову колекцію стрічок, кожна з яких є стрічкою, що містить
// від 6 до 15 символів). (написати метод)
//
//Продемонстуйте розпарсання дати, додавання/віднімання дати(дня, місяця, року) від поточної дати.
// (написати метод)

public class Main {


    public static void main(String[] args) {
        System.out.println("\n1) Generated random text:");
        System.out.println(new RandomStringGenerator().generateRandomStringList(5));

        System.out.println("\n2) Working with dates:");
        String textDate = "23/12/1982";
        Date currentDate = new Date();

        Date date = DateUtil.parseTextToDate(textDate);
        System.out.println(String.format("******* String \"%s\" to date: *******", textDate));
        System.out.println(date);
        System.out.println("******* Date to String: *******");
        System.out.println(String.format("\"%s\"", DateUtil.parseDateToString(currentDate)));

        System.out.println("******* Add and subtract dates: ****************");
        System.out.println(DateUtil.addToCurrentDate(Calendar.DAY_OF_MONTH, 10));
        System.out.println(DateUtil.addToCurrentDate(Calendar.DAY_OF_MONTH, -88));
        System.out.println(DateUtil.addToCurrentDate(Calendar.YEAR, 33));
        System.out.println(DateUtil.addToCurrentDate(Calendar.YEAR, -36));
        System.out.println(DateUtil.addToCurrentDate(Calendar.HOUR_OF_DAY, 1034));
        System.out.println(DateUtil.addToCurrentDate(Calendar.HOUR_OF_DAY, -189));
        System.out.println(DateUtil.addToCurrentDate(Calendar.MONTH, 10));
        System.out.println(DateUtil.addToCurrentDate(Calendar.MONTH, -5));
    }
}
