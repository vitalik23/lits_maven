package tasks9_10.task2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
    static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

    public static Date parseTextToDate(String text) {
        Date date = null;
        try {
            date = dateFormatter.parse(text);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String parseDateToString(Date date) {
        return dateFormatter.format(date);
    }

    public static Date addToCurrentDate(int dateType, int number) {
        Date currentDate = new Date();
//        Calendar calendar = Calendar.getInstance();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        calendar.add(dateType, number);
        return calendar.getTime();
    }

}
